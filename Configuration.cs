using System;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Windows.Forms;
using vis1.Properties;
using ZedHL;
using ZedGraph;
using System.Windows.Media;
using Brushes = System.Windows.Media.Brushes;

namespace vis1
{
    partial class VisForm3
    {
        #region Timing
        private const double FSample = 100;
        private const double Sample = 1 / FSample;
        private const int Disp = 100;  // milliSec
        private const int Thread = 20; // milliSec
        #endregion

	    private IValueSink []_ivsBuffer = new IValueSink[10];

        private bool _communicationIsOpen = false;

        //Konfigurationsdialog zur Auswahl eines COM-Ports
        bool ConfigCommunication()
        {
            if (!_communicationIsOpen)
            {
                var comport = "";
                var baudrate = "";
                var databits = "";
                Parity parity = Parity.None;
                StopBits stopbits = StopBits.One;
                var comDialog = new ChooseCom();    //Open window

                if (comDialog.ShowDialog(this) == DialogResult.OK)
                {
                    comport = comDialog.Port;       //Save selected COM-Port
                    baudrate = comDialog.Baud;      //Save selected BAUD-Rate
                    databits = comDialog.DataBits;  //Save selected Data Bits
                    switch(comDialog.Parity)        //Save selected Parity
                    {
                        case "None":
                            parity = Parity.None;
                            break;
                        case "Odd":
                            parity = Parity.Odd;
                            break;
                        case "Even":
                            parity = Parity.Even;
                            break;
                        case "Mark":
                            parity = Parity.Mark;
                            break;
                        case "Space":
                            parity = Parity.Space;
                            break;
                    }
                    switch (comDialog.StopBits)     //Save selected Stop Bits
                    {
                        case "One":
                            stopbits = StopBits.One;
                            break;
                        case "OnePointFive":
                            stopbits = StopBits.OnePointFive;
                            break;
                        case "Two":
                            stopbits = StopBits.Two;
                            break;
                    }
                    comDialog.Dispose();
                }

                if(comport == "")
                {
                    return false;
                }

                m_serialPort.PortName = comport;
                m_serialPort.BaudRate = Convert.ToInt32(baudrate);
                m_serialPort.DataBits = Convert.ToInt32(databits);
                m_serialPort.Parity = parity;
                m_serialPort.StopBits = stopbits;

                try
                {
                    m_serialPort.Open(); //Serielle verbindung �ffnen

                    m_protocolHandler = new ProtocolHandler(m_serialPort, this);    //Start ProtocolHandler
                    _communicationIsOpen = true;    //Declare Communicaton as open

                    return true;
                }
                catch (IOException )
                {
                    MessageBox.Show(@"IO Exception: " + comport + Resources.PortNotFound);
                    return false;
                }
                catch (Exception)
                {
                    MessageBox.Show(@"Exception: " + comport + Resources.PortNotFound);
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

	    void CloseCommunication()
	    {
            if (_communicationIsOpen)
            {
                m_serialPort.Close();
                m_protocolHandler.Close();

                _communicationIsOpen = false;
            }
	    }

        void SetupLineChart()
        {
            GraphElements.LineChart.GraphPane.Title.Text = "";

            GraphElements.LineChart.GraphPane.Y2Axis.IsVisible = true;

            //Apply X-Axis Values
            GraphElements.LineChart.GraphPane.XAxis.Scale.Max = GraphElements.x - ConfigObject.configuration.axis[0].min;
            GraphElements.LineChart.GraphPane.XAxis.Scale.Min = GraphElements.x - ConfigObject.configuration.axis[0].max;

            //Apply Y-Axis Values
            GraphElements.LineChart.GraphPane.YAxis.Scale.Max = ConfigObject.configuration.axis[1].max;
            GraphElements.LineChart.GraphPane.YAxis.Scale.Min = ConfigObject.configuration.axis[1].min;

            //Apply Y2-Axis Values
            GraphElements.LineChart.GraphPane.Y2Axis.Scale.Max = ConfigObject.configuration.axis[2].max;
            GraphElements.LineChart.GraphPane.Y2Axis.Scale.Min = ConfigObject.configuration.axis[2].min;

            GraphElements.LineChart.GraphPane.XAxis.Title.Text = "";
            GraphElements.LineChart.GraphPane.YAxis.Title.Text = "";

            zedGraphLineChart.AxisChange();

            //Create Curves and load Settings from app.config
            for (var track = 0; track < 9; track++)    
            {
                GraphElements.LineChart.GraphPane.AddCurve(ConfigObject.configuration.channels[track].name, 
                    new double[0],
                    new double[0],
                    System.Drawing.Color.FromName(ConfigObject.configuration.channels[track].color),
                    SymbolType.None);
                GraphElements.LineChart.GraphPane.CurveList[track].IsY2Axis = ConfigObject.configuration.channels[track].IsY2;
            }
        }

        void SetupBarChart()
        {
            GraphElements.BarChart.GraphPane.Title.Text = "";

            GraphElements.BarChart.GraphPane.Y2Axis.IsVisible = true;

            //Apply Y-Axis Values
            GraphElements.BarChart.GraphPane.YAxis.Scale.Max = ConfigObject.configuration.axis[1].max;
            GraphElements.BarChart.GraphPane.YAxis.Scale.Min = ConfigObject.configuration.axis[1].min;

            //Apply Y2-Axis Values
            GraphElements.BarChart.GraphPane.Y2Axis.Scale.Max = ConfigObject.configuration.axis[2].max;
            GraphElements.BarChart.GraphPane.Y2Axis.Scale.Min = ConfigObject.configuration.axis[2].min;

            GraphElements.BarChart.GraphPane.XAxis.Scale.Max = 0.5;
            GraphElements.BarChart.GraphPane.XAxis.Scale.Min = -0.5;

            GraphElements.BarChart.GraphPane.XAxis.IsVisible = false;
            GraphElements.BarChart.GraphPane.YAxis.Title.Text = "";

            zedGraphBarChart.AxisChange();

            //Create Curves and load Settings from app.config
            for (var track = 0; track < 9; track++)    
            {
                GraphElements.BarChart.GraphPane.AddBar(ConfigObject.configuration.channels[track].name,
                    null,
                    GraphElements.barData, 
                    System.Drawing.Color.FromName(ConfigObject.configuration.channels[track].color));
                GraphElements.BarChart.GraphPane.CurveList[track].IsY2Axis = ConfigObject.configuration.channels[track].IsY2;
            }
        }

        //TODO: Slider Wert senden
        public void OnValChanged(int aId, MSlider aSlider) // SliderCB  //Slider Wert senden
        {

        }

        public void OnMoseUp(int aId, MSlider aSlider) // SliderCB
        {
        }
    }
}